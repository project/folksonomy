DESCRIPTION
------------------
An API for module developers for enabling simple non hierarchical classification. This module enables tag based applications like Flickr [1] and Delicio.us [2]. This module doesn't actually do anything on its own. It will be used within photo gallery modules, bookmark modules, song modules, and more. Don't bother installing this module unless some other module instructs you to so.

This module is still immature. Module developers are encouraged to file feature requests and bug reports at http://drupal.org/project/folksonomy.

For now, folksonomy and taxonomy will live separate lives. It's plausible to fold folksonomy into taxonomy, if that becomes desirable. See the 'classify anything' issue: http://drupal.org/node/899. It's 2 years old (as of Dec 2004), so I'm not holding my breath.

[1] http://flickr.com/help.gne#37
[2] http://del.icio.us/doc/about


INSTALL
----------------
As usual, create the database tables and hen activate the module using admin/modules page.


Author
--------------
Moshe Weitzman <weitzman AT tejasaDOTcom>